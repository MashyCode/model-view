## model-viewer frontend
A build version of the Asaro model viewer. The build is hosted via Gitlab pages (forked from a simple HTML project). The development project will be linked here at a later date

---
![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

